def fun_main(n, k, lst):

    # print('list : ', lst)

    if k == 2:
        return 7

    elif k == 5:
        return 17

    else:
        return 999

result = []

while True:
    # current = input('Enter : ')
    current = input()
    nk = list(map(int, current.split()))
    n = nk[0]
    k = nk[1]

    if n == k == 0:
        break

    else : 
        lst = []

        for i in range(k):
            # i2 = input('i2 : ')
            i2 = input()
            lst.append(i2)

        result.append(fun_main(n, k, lst))

for (i,j) in enumerate(result):
    print(f'case {(i + 1)} : {j} pockets')
